
// bolean

let estarAtivo: false

estarAtivo = false

function ligarDesligar(status: boolean) {
    if (status){
        console.log('esta ligado')
    }else{
        console.log('esta desligado')
    }
}

ligarDesligar(false)
ligarDesligar(true)


// list

const listNumber: string[] = ['1', '2', '3', '4', '5', '6']

function viewGet(numbs: string[]){
    console.log(`${numbs.join(', ')}`)
}

viewGet(listNumber)

// list[]

const developer: [string,string,string] = [
    'coffee',
    'coffee',
    'coffee',
]

const coffee: [string, number] = [
    'coffee', 1
]

// emun

enum Permissoes {
    admin,
    editor,
    comum
}

enum colors {
    red='#ff0000',
    black='#000'
}

const user = {
    nivel: Permissoes.admin,
    profile: colors.red
}

console.log(user)

//any

let valorNumero: any

valorNumero = 25
valorNumero = '25'

function soma(a: any, b:any) {
    return a + b
}

// unknown

//let informacao: unknown = 1234
//let informacaoComplets: string
//
//informacaoComplets = informacao

