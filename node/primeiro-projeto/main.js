const http = require('http')
const url = require('url')
const queryString = require('query-string')

const server = http.createServer((req, res) => {

    const params = queryString.parse(url.parse(req.url, true).search)

    res.statusCode = 200
    res.setHeader('Content-Type', 'application/json')
    res.end(params.foo)
})

server.listen(3333, '127.0.0.1', () => {
    console.log('http://127.0.0.1:3333')
})

