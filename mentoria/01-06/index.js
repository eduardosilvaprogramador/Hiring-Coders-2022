import axios from "axios"
import fetch from "node-fetch"

//(async function foo() {
//    const res = await fetch("https://rickandmortyapi.com/api/character")
//
//    const data = await res.json()
//    console.log(data)
//})()

// (async () => {
//     const res = await axios.get("https://rickandmortyapi.com/api/character")
//     console.log(res.data)
// })()

(async () => {
    const body = { text: 'hello'}
    const res = await fetch('https://api.funtranslations.com/translate/yoda', {
        method: 'POST',
        body: JSON.stringify(body),
        headers: {'Content-Type': 'application/json'}
    })

    const data = await res.json()
    console.log(data)

})()

// (async () => {
//
// })()
