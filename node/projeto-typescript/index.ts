import { createServer, IncomingMessage, ServerResponse} from 'http'
import { parse } from 'query-string'
import url from 'url'
import { writeFile } from 'fs'

const server = createServer((req: IncomingMessage, res: ServerResponse) => {
    const urlparse = url.parse(req.url ? req.url : '', true)

    const params = parse(urlparse.search ? urlparse.search : '')

    let resp

    if(urlparse.pathname == '/create-user') {
        writeFile(`users/${params.id}.txt`, JSON.stringify(params), (err: any) => {
            if (err){
               throw err
            }

            resp = 'User Create'

            res.statusCode = 200
            res.setHeader('Content-Type', 'text/plain')
            res.end(resp)
        })
    }
})

server.listen(3000)

