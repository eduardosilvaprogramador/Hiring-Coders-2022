"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = require("http");
const query_string_1 = require("query-string");
const url_1 = __importDefault(require("url"));
const fs_1 = require("fs");
const server = (0, http_1.createServer)((req, res) => {
    const urlparse = url_1.default.parse(req.url ? req.url : '', true);
    const params = (0, query_string_1.parse)(urlparse.search ? urlparse.search : '');
    let resp;
    if (urlparse.pathname == '/create-user') {
        (0, fs_1.writeFile)(`users/${params.id}.txt`, JSON.stringify(params), (err) => {
            if (err) {
                throw err;
            }
            resp = 'User Create';
            res.statusCode = 200;
            res.setHeader('Content-Type', 'text/plain');
            res.end(resp);
        });
    }
});
server.listen(3000);
