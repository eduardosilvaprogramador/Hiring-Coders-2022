let element = []
let top = -1
const max = 10

function push(num) {
    if (top < max) {
        top++
        element[top] = num
    }
    else {
        console.log('Cheia')
    }
}

function elementNull() {
    return top == -1
}

function pop() {
    if (top != -1){
        let num = element[top]
        top--
        return num
    }
    else {
        console.log('Vazia')
    }
}

push(30)
push(20)
push(10)

console.log(element)

console.log(pop())
console.log(pop())
console.log(pop())

console.log(element)

let numDecimal = 10
let resto

while (numDecimal != 0) {
    resto = parseInt(numDecimal % 2)
    push(resto)
    console.log(resto);

    numDecimal = parseInt(numDecimal / 2)
}

while (!elementNull()) {
    console.log(pop())
}

