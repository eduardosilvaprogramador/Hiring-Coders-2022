import { createServer } from 'http'
import { readFile } from 'fs'
import { resolve } from 'path'
import { parse } from 'querystring'

const server = createServer((req, res) => {
    switch (req.url){
        case '/status': {
            res.writeHead(200, {
                'Content-Type': 'application/json',
            })
            res.write(
                JSON.stringify({
                    status: 'Ok'
                })
            )
            res.end()
            break
        }

        case '/sign-in': {
            const filePath = resolve(__dirname, './pages/sign-in.html')

            readFile(filePath, (error,file) => {
                if(error) {
                    res.writeHead(500, 'process file HTML')
                    res.end()
                    return
                }

                res.writeHead(200)
                res.write(file)
                res.end()
            })

            break
        }

        case '/home': {
            const filePath = resolve(__dirname, './pages/home.html')

            readFile(filePath, (error,file) => {
                if(error) {
                    res.writeHead(500, 'process file HTML')
                    res.end()
                    return
                }

                res.writeHead(200)
                res.write(file)
                res.end()
            })

            break
        }

        case '/authenticate': {
            let data = ''
            req.on('data', (chunk) => {
                data += chunk
            })
            req.on('end',() => {
                const params = parse(data)
                res.writeHead(301, {
                    Location: '/home'
                })
                res.end()
            })
            break
        }

        default : {
            res.writeHead(400, 'notSound')
            res.end()
        }
    }
})

server.listen(3333, '127.0.0.1', () => {
    console.log('hello world! http://127.0.0.1:3333')
})

