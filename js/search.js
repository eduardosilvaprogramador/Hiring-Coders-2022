const value = [12,43,56,87,97]

function search(num) {
    for (let index in value){
        if(num == value[index]) return index
    }
    return -1
}

console.log(search(22))
console.log(search(12))
console.log(search(56))
console.log(search(99))

// aula 6 busca binaria


function searchBin(num) {
    let init = 0 
    let end = 9
    let steps = 0
    let quite

    while(init<=end){
        quite = parseInt((init + end) / 2)
        steps++
        if (num == value[quite]){
            console.log('Em: '+ steps+' Passos.')
            return quite
        } else if (num > value[quite]) {
            init = quite + 1
        } else {
            end = quite - 1
        }
    }

    console.log(`não encontrado ${steps}`)
    return -1
}

console.log('.: busca binaria :.')

console.log('.: '+value+':.')


console.log('43',searchBin(43))
console.log('12',searchBin(12))
console.log('50',searchBin(50))
console.log('87',searchBin(87))
console.log('97',searchBin(97))

