import Pagination from '@material-ui/lab/Pagination'

interface IPropsComponent {
    handleChange:
        | ((evnet: React.ChangeEvent<unknown>, page: number) => void)
        | undefined;
    page: number;
    total_page: number;
}

export default function PaginationComponent({
    handleChange,
    page,
    total_page
}: IPropsComponent) {
    return (
        <>
            <Pagination
                count={total_page}
                color="secondary"
                onChange={handleChange}
            />
        </>
    )
}

