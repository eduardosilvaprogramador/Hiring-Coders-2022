'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.createTable('users', {
      id: {
        type: Sequelize.INTERGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      name:{
        type: Sequelize.STRING,
        AllowNull: false
      },
      email:{
        type: Sequelize.STRING,
        AllowNull: false
      },
      password_hash:{
        type: Sequelize.STRING,
        AllowNull: false
      },
      provider:{
        type: Sequelize.BOLEAN,
        defaultValue: false,
        AllowNull: false
      },
      created_at:{
        type: Sequelize.DATE,
        AllowNull: false
      },
      updated_at:{
        type: Sequelize.DATE,
        AllowNull: false
      },
    })
  },

  async down (queryInterface) {
    return queryInterface.dropTable('users')
  }
};

