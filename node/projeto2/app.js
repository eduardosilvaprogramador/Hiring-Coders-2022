import url from 'url'
import http from 'http'
import fs from 'fs'

import queryString from 'query-string'

const testserver = http.createServer((req, res) => {

    let resp
    const url_parser = url.parse(req.url, true)
    const params = queryString.parse(url_parser.search)

    if(url_parser.pathname == '/create-user') {
        fs.writeFile(`users/${params.id}.txt`, JSON.stringify(params), err => {
            if (err){
               throw err
            }

            resp = 'User Create'

            res.statusCode = 200
            res.setHeader('Content-Type', 'application/json')
            res.end(resp)
        })

    } else if(url_parser.pathname == '/select-user'){
        fs.readFile(`users/${params.id}.txt`, (err, data) => {
            resp = data

            res.statusCode = 200
            res.setHeader('Content-Type', 'text/plain')
            res.end(resp)
        })

    } else if(url_parser.pathname == '/delete-user'){
        fs.unlink(`users/${params.id}.txt`, (err) => {
            console.log('user delete')

            resp = err ?  "User not sound" : 'User remove'

            res.statusCode = 200
            res.setHeader('Content-Type', 'text/plain')
            res.end(resp)
        })
    }
})

testserver.listen(3000, '127.0.0.1')

