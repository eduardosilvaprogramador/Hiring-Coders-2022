import { GetServerSideProps, GetStaticProps } from "next";

import api from '../service/apii.json'
import { Container } from '../styles/DashBoard/styles'

interface ICategories {
    id: number;
    title: string
}

interface CategoryProps {
    categories: ICategories[]
}

export default function categories({ categories}: CategoryProps) {
    return (
        <Container>
            <h1>Categorias</h1>
            <section>
                <ul>
                    {categories.map(categorie => {
                        return (
                            <li key={categorie.id}>
                                {categorie.title}
                            </li>
                        )
                    })}
                </ul>
            </section>
        </Container>
    )
}

export const getStaticProps: GetStaticProps<CategoryProps> = async () => {
    const res = await api.get('http://localhost:3333/categories')
    const categories = await res.data 

    return {
        props: {
            categories
        },
        revalidate: 5
    }
}
