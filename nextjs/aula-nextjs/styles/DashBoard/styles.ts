import styled from 'styled-components'

export const container = styled.div `
    height: 100vh;
    width: 100%;
    align-items: center;
    justify-content: center;
    flex-direction: column;
`;

export const Form = styled.form `
    width: 50%;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    input {
        margin-right: 20px;
        width: 100%;
        height: 50px;
        border-radius: 10px;
    }
    button {
        height: 40px;
    }
`;

export const Repositories = styled.div `
    margin-top: 80px;
    max-width: 700px;
    section {
        margin: 0 16px;
        flex: 1;
        img {
            width: 60px;
            height: 60px;
        }
    }
`;
