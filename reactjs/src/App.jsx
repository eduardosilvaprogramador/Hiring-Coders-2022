import {useState} from 'react'
import axios from 'axios'

function App() {
  const [ usuario, setUsuario ] = useState('')
  console.log(usuario);

  function handlePesquisa() {
    axios.get(`https://api.github.com/users/${usuario}/repos`).then(res => {
      console.log('foo')

    })
  }

  return (
    <>
      <input className="usuarioInput" placerholder="Usuário" value={usuario} onChange={ e => setUsuario(e.target.value)}/>
      <button type="button" onChange={handlePesquisa}>Pesquisar</button>
    </>
  )
}

export default App

