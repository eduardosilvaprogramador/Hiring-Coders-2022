import {FormEvent, useEffect, useState} from 'react'

import { useRouter } from 'next/router'

import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import Pagination from '../comp/Pagination'
import { args } from '../config/api'

interface IPropsComponent {
  list: any[]
  page: number
  total_page: number
  search: boolean
  searchParam: string
}


const Home = ({list,page,total_page, searchParam} :
  IPropsComponent) => {

  const [data, setData] = useState<any[]>([])
  const router = useRouter()

  const [search, setSearch] = useState(searchParam)

  const [result, setResult] = useState<undefined | string>(undefined)

  const handleChange = (evnet: React.ChangeEvent<unknown>, value: number) => {
    if(search){
      return router.push(`?search=${search}&page=${value}`)
    } else {
      return router.push(`?page=${value}`)
    }
  }
  async function handleSearchMovie(e: FormEvent<HTMLFormElement>) {
    e.preventDefault()
    return router.push(`/?search=${search}&page=1`)

  }
}

export default Home

export async function getServerSideProps({
  query
}: {
  query: {
    page?:string
    serach?: string
  }
}) {
  if(query.serach) {
    const res = await fetch(
      `${args.base_url}/search/movie?api_key${args.api_key}&query=${query.serach}&page=${query.page ? query.page : 1}&language=pt-BR`
    )
    const { results, page, total_page } = (await res.json()) as any

    return {
      props: {
        list: results, page, total_page, searchParam: query.serach

      }
    }
  }else {

    const res = await fetch(

      `${args.base_url}/trending/movie/week?api_key=${args.api_key}&page=${query.page ? query.page : 1}language=pt-BR`
    )

    const { results, page, total_pages } = (await res.json()) as any

    return {
      props: {
        list: results,
        page: page,
        total_pages: total_pages,
        searchParam: ""
      }
    }
  }
}

